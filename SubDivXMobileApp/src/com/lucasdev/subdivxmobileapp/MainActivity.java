package com.lucasdev.subdivxmobileapp;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
public class MainActivity extends ActionBarActivity {
	private final String ASSETS_PREFIX = "file:///android_asset/";
	private static final String AD_UNIT_ID = "ca-app-pub-9325261703865037/6285985305";
	private InterstitialAd interstitial;
	private AdRequest adRequest;
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        if (savedInstanceState == null) {
            addFragment("index.html");
        }
        
        setUpInterstitial();
    }
 
	private void setUpInterstitial() {
	// Create the interstitial.
	    interstitial = new InterstitialAd(this);
	    interstitial.setAdUnitId(AD_UNIT_ID);
	    interstitial.setAdListener(new AdListener() {
	        @Override
	        public void onAdLoaded() {
	        	interstitial.show();
	        }

	    });
	    
	 // Check the logcat output for your hashed device ID to get test ads on a physical device.
	    adRequest = new AdRequest.Builder()
//	        .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
//	        .addTestDevice("65C17FA269B33D1372C81D6D543C5CF4")
	        .build();
	}

	private void addFragment(final String html) {
		PlaceholderFragment f = new PlaceholderFragment();
		f.setInterface(new com.lucasdev.subdivxmobileapp.MainActivity.PlaceholderFragment.WebViewFragmentInterface() {
			
			@Override
			public void reloadWV(final WebView wv) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						setUpWebView(wv, "www/"+html);
					}
				});
			}
		});
		getSupportFragmentManager().beginTransaction()
				.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
		        .add(R.id.container, f)
		        .addToBackStack(null)
		        .commit();
	}
    
    public static class PlaceholderFragment extends Fragment {
    	private WebViewFragmentInterface wvi;
    	private interface WebViewFragmentInterface{
    		public void reloadWV(WebView wv);
    	}
        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            WebView wv = (WebView)rootView.findViewById(R.id.webView);
            wvi.reloadWV(wv);
            return rootView;
        }
        
        public void setInterface(WebViewFragmentInterface wvi){
        	this.wvi = wvi;
        }
    }
    
    
	private void fixRenderWV(WebView wv) {
		try {
		    Method setLayerTypeMethod = wv.getClass().getMethod("setLayerType", new Class[] {int.class, Paint.class});
		    setLayerTypeMethod.invoke(wv, new Object[] {View.LAYER_TYPE_SOFTWARE, null});
		} catch (NoSuchMethodException e) {
		    // Older OS, no HW acceleration anyway
		} catch (IllegalArgumentException e) {
		    e.printStackTrace();
		} catch (IllegalAccessException e) {
		    e.printStackTrace();
		} catch (InvocationTargetException e) {
		    e.printStackTrace();
		}
	}
	private void allowUniversal(WebSettings settings) {
		try{
			Method m9 = WebSettings.class.getMethod("setAllowUniversalAccessFromFileURLs", new Class[]{Boolean.TYPE});
			m9.invoke(settings, Boolean.TRUE);			
		}catch(Exception e){
			
		}
	}
	
	public void setUpWebView(WebView vw, String url) {
		vw.setWebChromeClient(new WebChromeClient(){
			@Override
			public boolean onConsoleMessage(ConsoleMessage cm) {
				Log.d("SUBDIVX console", String.format("%s @ %d: %s", 
	                    cm.message(), cm.lineNumber(), cm.sourceId()));
	        return true;
			}
		});
		fixRenderWV(vw);
		fixLayoutGingerbread(vw);
		WebSettings settings = vw.getSettings();
		settings.setJavaScriptEnabled(true);
		allowUniversal(settings);
		vw.loadUrl(ASSETS_PREFIX + url);
		vw.addJavascriptInterface(this, "Android");
	}

	private void fixLayoutGingerbread(WebView vw) {
		getSupportActionBar().hide();
		vw.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
		vw.requestFocus(View.FOCUS_DOWN);
	}
	
	@JavascriptInterface
	public void goNextScreen(String html){
		addFragment(html);
	}
	@JavascriptInterface
	public void goBack(){
		FragmentManager fm = getSupportFragmentManager();
		if (fm.getBackStackEntryCount() > 1) {
		    fm.popBackStack();
		} else {
		    finish();  
		}
	}
	
	@Override
	public void onBackPressed() {
		goBack();
	}
	
	@JavascriptInterface
    public void displayInterstitial() {
	  runOnUiThread(new Runnable() {
		 @Override
		 public void run() {
			interstitial.loadAd(adRequest);
		 }
	  });
    }
	
	@JavascriptInterface
	public void goToIndex(){
		finish();
		startActivity(new Intent(MainActivity.this, MainActivity.class));
	}
}
