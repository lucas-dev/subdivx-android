var showMenu = false;

$(document).ready(function() {
    showMenu = false;
    var irroba = $('<div id="spinner" class="spinner"><img src="img/ajax-loader.gif" /><p id="p-spinner-msg"></p></div>');
    $(document.body).append(irroba);
    
});


function showActivityIndicator(msg){
    $("#p-spinner-msg").text(msg);
    $('#spinner').show();
}

function hideActivityIndicator(){
    $('#spinner').hide();
}

var isMobile = {
    Android: function() {return navigator.userAgent.match(/Android/i);},
    BlackBerry: function() {return navigator.userAgent.match(/BlackBerry/i);},
    iOS: function() {return navigator.userAgent.match(/iPhone|iPad|iPod/i);},
    Opera: function() {return navigator.userAgent.match(/Opera Mini/i);},
    Windows: function() {return navigator.userAgent.match(/IEMobile/i);},
    any: function() {return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());}
};

var endpoints = {
	index : "http://www.subdivx.com/index.php",
	popup : "http://www.subdivx.com/popcoment.php",
	base  : "http://www.subdivx.com/"
}
var fullRequestURL = "";

function ajaxRequest(pUrl,pParams,pType,pCallback){
	var par = isNull(pParams)?"NO PARAMS":JSON.stringify(pParams);
	console.log("Request to "+pUrl+" with params: "+par);
	if(!containsIgnoreCase(pUrl, endpoints.popup)){
		fullRequestURL = getFullUrlFromParams(pUrl, pParams);
	    $("#a-current-page").attr("href",fullRequestURL);
	}
    $.ajax({
        url:pUrl,
        data:pParams,
        type:pType}
    ).done(function(data){
		pCallback(data);

	}).fail(function (jqxhr) {
        console.log(JSON.stringify(jqxhr));
        getErrorArea();
    });
}

/**
 * Verifica la existencia de un string dentro de otro ignorando mayusculas y minusculas
 * @param {string} src - la cadena en la que se debe buscar
 * @param {string} value - la cadena a buscar
 * @return {boolean} true si src contiene value, false sino
 */

function containsIgnoreCase(src, value){
    return (src.toUpperCase()).indexOf(value.toUpperCase()) != -1;
}

function newScreen(html){
	if(isMobile.iOS()){
    }else if(isMobile.Android()){
    	Android.goNextScreen(html);
    }else if(isMobile.Windows()){
    }else if(isMobile.BlackBerry()){
    }else{
    	window.location.href=html;
    }
}

function goBack(){
    if(isMobile.iOS()){
    }else if(isMobile.Android()){
    	Android.goBack();
    }else if(isMobile.Windows()){
    }else if(isMobile.BlackBerry()){
    }else{
        history.back();
    }
}

function goBackToIndex(){
    if(isMobile.iOS()){
    }else if(isMobile.Android()){
    	Android.goToIndex();
    }else if(isMobile.Windows()){
    }else if(isMobile.BlackBerry()){
    }else{
        history.back();
    }
}

function showAd(){
    if(isMobile.iOS()){
    }else if(isMobile.Android()){
    	Android.displayInterstitial();
    }else if(isMobile.Windows()){
    }else if(isMobile.BlackBerry()){
    }else{
        history.back();
    }
}

function getParams(){
    var urlParams;
    (window.onpopstate = function () {
        var match,
        pl     = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); };
        if(isMobile.Windows())
        {
            query  = window.location.hash.substring(1);
        }else{
            query  = window.location.search.substring(1);
        }
        
        urlParams = {};
        while (match = search.exec(query))
        urlParams[decode(match[1])] = decode(match[2]);
    })();
    return urlParams;
}

function getStringFromDOMEl(expres){
	try{
		return expres();
	}catch(e){
		return "-";
	}
}


jQuery.expr[":"].Contains = jQuery.expr.createPseudo(function(arg) {
    return function( elem ) {
        return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    };
});

/**
 * Will check for empty strings (""), null, undefined, false and the numbers 0 and NaN
 * @param {string} value
 * @return {boolean} true si el parametro es valido
 */
function isNull(value){
    return !value;
}


function checkEmptyDict(obj){
    for(var i in obj){ return false;}
    return true;
}


function showModalDialog(title, closeMessage, content){
   var documentoHTML = $('<div id="open-modal" class="modal-window"><div><header>'+title+'</header><div class="modal-content"><a href="#modal-close" title="Close" class="modal-close">'+closeMessage+'</a><div>'+content+'</div></div></div>'); 
  $(document.body).append(documentoHTML);
  
  $(".modal-close").click(function(){
    $("#open-modal").remove();
  });
}

function getLiComment(message,userLink, userName){
    return '<li><div class="container"><div class="padding">'.
    concat('<article class="module"><div class="bar-full">').
    concat('<span class="bar"></span><div class="bar-box"></div>').
    concat('</div><div class="pad"><p style="text-align:right; padding: 0 10px 0 0;">').
    concat(message+'<br>Por: <a href="'+userLink+'" class="user-link">'+userName+'</a></p>').
    concat('</div></article></div></div></li>');
}

function appendNavHeader(title, showUrl){
    var html = '<div class="navbar"><div class="back"><img src="img/back-icon.png"></div> <div class="title"><h1>'+title+'</h1></div>'.
                concat('   <div class="menu"><img src="img/menu-icon.png"></div></div><div id="navigation"><ul><li><a href="index.html" class="redirect-index">Inicio</a></li>').
                concat('<li><a href="subs-list.html" class="redirect">Búsqueda Clásica</a></li><li><a href="power-search.html" class="redirect">Super Búsqueda</a></li>');
                if(showUrl)
                    html+=('<li><a href="http://www.subdivx.com" id="a-current-page" class="redirect-current-page">Ir a la página actual</a></li>');
                html+=('</ul></div>');
    $(document.body).prepend($(html));

    /*toggle menu*/
    $(".menu").click(function(){
        showMenu = !showMenu;
        if(showMenu){
            $("#navigation").show();
            $(this).find("img").attr("src","img/x-icon.png");
        }else{
            $("#navigation").hide();
            $(this).find("img").attr("src","img/menu-icon.png");
        }
    });

    $(".back").click(function(){
      goBack();
    });
}


function getFullUrlFromParams(url, params){
    var fullUrl = url;
    for(i in params){
        fullUrl+=(resolveURLDelimiter(fullUrl)+i+"="+params[i]);
    }

    return fullUrl;
}

function resolveURLDelimiter(url){
    return containsIgnoreCase(url, "?")?"&":"?";

}

function getErrorArea(url){
    var error = '<div id="div-error">'.
        concat('<p class="error-title">Ha ocurrido un error obteniendo información del servidor de subdivx.com</p>').
        concat('<p class="error-mas-info">Más información en:</p>').
        concat('<p class="error-mas-info"><a class="error-msg" href="'+fullRequestURL+'">'+fullRequestURL+'</a></p>').
        concat('<img src="img/bug.png"></div>');
    hideActivityIndicator();
    $(".div-main-container").replaceWith(error);

}